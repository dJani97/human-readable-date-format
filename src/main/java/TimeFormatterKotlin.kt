import kotlin.collections.MutableList

object TimeFormatterKotlin {

  // time units in seconds
  private const val SECOND = 1
  private const val MINUTE = 60 * SECOND
  private const val HOUR = 60 * MINUTE
  private const val DAY = 24 * HOUR
  private const val YEAR = 365 * DAY

  // second to unit name mapping
  private val secondsToUnit = mapOf(
      YEAR to "year",
      DAY to "day",
      HOUR to "hour",
      MINUTE to "minute",
      SECOND to "second"
  )

  private fun pronounce(unitName: String, occurrences: Int): String
      = if (occurrences == 1) "$occurrences $unitName"
  else "$occurrences ${unitName}s"


  fun formatDuration(seconds: Int): String {
    if (seconds == 0)
      return "now"

    var remainingSeconds = seconds
    val expressions: MutableList<String> = ArrayList()

    secondsToUnit.forEach {(secondsPerUnit, unitName) ->
      if (remainingSeconds >= secondsPerUnit) {
        val occurrences = remainingSeconds / secondsPerUnit
        expressions.add(pronounce(unitName, occurrences))
        remainingSeconds -= occurrences * secondsPerUnit
      }
    }

    if (expressions.size < 2)
      return expressions.first()

    expressions.joinToString(", ").apply {
      return substringBeforeLast(", ") +
          " and " +
          substringAfterLast(", ")
    }
  }
}

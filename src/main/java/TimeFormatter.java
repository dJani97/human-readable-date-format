import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class TimeFormatter {

  // time units in seconds
  private static final int SECOND = 1;
  private static final int MINUTE = 60 * SECOND;
  private static final int HOUR   = 60 * MINUTE;
  private static final int DAY    = 24 * HOUR;
  private static final int YEAR   = 365 * DAY;

  // second to unit name mapping
  private static final Map<Integer, String> secondsToUnit = buildSecToUnitMap();

  private static Map<Integer, String> buildSecToUnitMap() {
    Map<Integer, String> secToUnit = new LinkedHashMap<>();
    secToUnit.put(YEAR, "year");
    secToUnit.put(DAY, "day");
    secToUnit.put(HOUR, "hour");
    secToUnit.put(MINUTE, "minute");
    secToUnit.put(SECOND, "second");
    return secToUnit;
  }

  private static String pronounce(String unitName, int occurrences) {
    if (occurrences == 1) return String.format("%s %s", occurrences, unitName);
    return String.format("%s %ss", occurrences, unitName);
  }

  public static String formatDuration(final int seconds) {
    if (seconds == 0) {
      return "now";
    }

    int remainingSeconds = seconds;
    List<String> expressions = new ArrayList<>();
    for (int secondsPerUnit : secondsToUnit.keySet()) {
      if (remainingSeconds >= secondsPerUnit) {
        int occurrences = remainingSeconds / secondsPerUnit;
        expressions.add(pronounce(secondsToUnit.get(secondsPerUnit), occurrences));
        remainingSeconds -= occurrences * secondsPerUnit;
      }
    }

    if (expressions.size() == 1)
      return expressions.get(0);

    StringBuilder result = new StringBuilder(expressions.get(0));
    for (int i = 1; i < expressions.size(); i++) {
      if (i != expressions.size() - 1)
        result.append(", ");
      else
        result.append(" and ");
      result.append(expressions.get(i));
    }

    return result.toString();
  }
}

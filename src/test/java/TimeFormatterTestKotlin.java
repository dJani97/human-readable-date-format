import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TimeFormatterTestKotlin {
    @Test
    public void exampleTests() {
        assertEquals("now", TimeFormatterKotlin.INSTANCE.formatDuration(0));
        assertEquals("1 second", TimeFormatterKotlin.INSTANCE.formatDuration(1));
        assertEquals("1 minute and 2 seconds", TimeFormatterKotlin.INSTANCE.formatDuration(62));
        assertEquals("2 minutes", TimeFormatterKotlin.INSTANCE.formatDuration(120));
        assertEquals("1 hour", TimeFormatterKotlin.INSTANCE.formatDuration(3600));
        assertEquals("1 hour, 1 minute and 2 seconds", TimeFormatterKotlin.INSTANCE.formatDuration(3662));
    }
}
